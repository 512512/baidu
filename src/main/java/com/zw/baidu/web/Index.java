package com.zw.baidu.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 创建人:zhuwei
 * 邮箱:121203654@qq.com
 * 创建时间:2018-07-27 16:54
 */
@Controller
public class Index {

    @RequestMapping("/")
    public String Index(){
        return "html";
    }
}
