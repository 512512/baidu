package com.zw.baidu.web;


import com.zw.baidu.BaiduApplication;
import com.zw.baidu.util.BaiDuUtils;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * 创建人:zhuwei
 * 邮箱:121203654@qq.com
 * 创建时间:2018-07-27 11:42
 */
@RestController
@RequestMapping("api")
public class IndexController {
    @PostMapping("orcUrl")
    public @ResponseBody Object textOrcUrl(String url){
        System.out.println(url);
        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("language_type", "CHN_ENG");
        options.put("detect_direction", "true");
        options.put("detect_language", "true");
        options.put("probability", "true");
        JSONObject res= BaiduApplication.aipOcr.basicGeneral(BaiDuUtils.getImageFromNetByUrl(url), options);
        return res.toMap();
    }
    //处理文件上传
    @PostMapping(value="/testuploadimg")
    public @ResponseBody String uploadImg(@RequestParam("file") MultipartFile file,
                                          HttpServletRequest request) {
        String contentType = file.getContentType();
        String fileName = file.getOriginalFilename();
        /*System.out.println("fileName-->" + fileName);
        System.out.println("getContentType-->" + contentType);*/
        System.out.println(fileName);
        System.out.println(contentType);
        try {

        } catch (Exception e) {
            // TODO: handle exception
        }
        //返回json
        return "uploadimg success";
    }
}
